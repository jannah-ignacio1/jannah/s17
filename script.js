/*1. In the S17 folder, create an activity folder and an index.html and script.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create an addStudent() function that will accept a name of the student and add it to the student array.
4. Create a countStudents() function that will print the total number of students in the array.
5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
6. Create a findStudent() function that will do the following:
Search for a student name when a keyword is given (filter method).
- If one match is found print the message studentName is an enrollee.
- If multiple matches are found print the message studentNames are enrollees.
- If no match is found print the message studentName is not an enrollee.
- The keyword given should not be case sensitive.
7. Create a git repository named S17.
8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
9. Add the link in Boodle.*/

let studentName = [];
function addStudent(name) {
	studentName.push(name)
	console.log(name	 + " was added to the student's list.")
}

function countStudents() {
	let numOfStuds = studentName.length
	console.log("There are a total of " + numOfStuds + " students enrolled.")
}

function printStudents() {
	studentName.sort()
	studentName.forEach(
		function (string) {
			console.log(string)
		}
	)
}


let findResult = []
function findStudent(a) {
	findResult = studentName.filter(e => e.toLowerCase().includes(a))
	const resultLen = findResult.length
		switch (true) {
			default: return a + " is not enrolled"; break;
			case resultLen === 1: return a + ' is enrolled'; break
			case resultLen > 1: return studentName + ' are enrollees';break
		}

			}


	




/*Stretch Goals:
1. Create an addSection() function that will add a section to all students in the array with the format of studentName - Section A (map method).
2. Create a removeStudent() Function that will do the following:
- Capitalize the first letter of the user’s input (toUpperCase and slice methods).
- Retrieve the index of the student to be removed (indexOf method).
- Remove the student from the array (splice method).
- Print a message that the studentName was removed from the student’s list.*/